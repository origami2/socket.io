var assert = require('assert');
var StackIO = require('../stack-io');
var o2 = require('origami2-core');

var testData = {
  "plugin": {
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAiLbPGJrBin4pKe8iHYHnU2ii0MPbwuGqwQLU+ymTGNXc8DPC\nmR31TJ5FAifzndatPdSLy9WgciIzwDySpsklMfJ6uDLm2ecEfTfjYGeK3VJuppS6\n7C08YAHquEAm6ZpxHqX955Hc3LuORYldpp281zZQ02+UizqeqbNBqEiuIMiIldvF\n9ARu5wyL0R7vwHqDAyEt8+7zd4rYmidmLvXwwqak/gpYnD0NqMB/GIFezoDLUGVi\nPlhtpImfj3eJEmi6v8FRpy/DiDmxPHXLoTl8qdDzcr1jsrlMD3UMj9hZnNw+YaUI\nNoYL8yJBL3gRkSIjBbxHSgZkpxdGGMThtAwFPQIDAQABAoIBAQCFX9Mqbc/jDLGb\n92+Q2eqTtMEiafE7bmznX5voa+bVk863h8eRGWj2FnG8aL2Cfq0EZRb9BKxCrJtQ\nL8fibtmaoLSYfxA4BOicQKnSozEWOhU47mBtiLQd9cDHf852l/VDsSzDfcm6jj9+\nF0UGB1zzmEnB/Fkue0HOzNKBGmbGE1/RWo4+co3XYpq/YZdIQQadsV6G2ma1IApm\n5t9AmEzhjMJsVFTZqzWQpoHmJRJjfwqqugmt4gpBCuYMkVcuN0g1olOLobF+pITn\nNjVaYimiFDsjhZyM4c0MFUYB27TeG7RV+QigHM/DW9rTbvTXxnTl/O4mipUe4FsK\nTdsbu4kBAoGBAPwJeudZz0cYM7MPzYE0/ksA9OrJwbhg+BjUJQ7CtFijJp8Cxt3d\ncu6DC5GOke0dXe845DLwriYKqJbae0FIeQvHrFTRpC2gGI5CJ3nPrZ3jbJhQLnQA\nMoF7EbJdpMM0I1xZsZ1cOy7jDbQ6/5Za4RC59xyqZ8hIi8vn9JVAkurPAoGBAIrd\nHyQ0NuZsQCxqNKPvU+1QnpFaoy65yA1kLZ/JDghjTiplxmv0WMuf1B3rq09RvTKK\nCEc7S8CredzLDtgQF7KiENnMno9e8bcMAddC+UaKsV9Eoy4mXGe8vTnQsGvlMkZI\n27gkM1o+Sbsl45Ayr3eYcpJD6K+txSjJYO8B3GIzAoGAUQ5ndYIftHinH95kNDqr\n0clj+yKZ58df4vRPWrjpsVv/LsKA3Je8v9JrZQuaCM0aCbadRXi8OUXSRHnNjAhX\nzZ8Q4FJv37COVSoXcgiFiLK8mRuoZOwvUg8XeOq+83yQJsI96iLgccrZ/G3BB0UA\n/xUf0RtIt1QFibV2po2W8mcCgYBjLOXO35POIcX7cqbB5m3Ucd2uBkPBXWIpXkDP\ne7KP/wyWbzW1aD/6vd2quOQStFghvj+HUCwcINvZ+xRQ771dES5jvyYHU1Hi36p4\n6RZLcUaYudapYTBhzoR+xDMb/AdZ9zMlYoVikFXsWXUbSXfUPIanO+T1g2/qX1jh\nmjyhzQKBgQCvQdWSYMXaOnQRo/BHmRdR3X3aEZyLEn5UmXjxohUeB/Jtzb15FD/r\ny5ljkTVQyzYnRgwBuZBnUl/sCbxRp1CXbDmotHZ3DBwRJuP7KaPoj7dH+xfU6P3N\nuAm6CP+73AONmouW/8tVW60n2IkSH5I1ROYtPLrI6475M/hB4Anj+Q==\n-----END RSA PRIVATE KEY-----",
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAiLbPGJrBin4pKe8iHYHnU2ii0MPbwuGqwQLU+ymTGNXc8DPCmR31\nTJ5FAifzndatPdSLy9WgciIzwDySpsklMfJ6uDLm2ecEfTfjYGeK3VJuppS67C08\nYAHquEAm6ZpxHqX955Hc3LuORYldpp281zZQ02+UizqeqbNBqEiuIMiIldvF9ARu\n5wyL0R7vwHqDAyEt8+7zd4rYmidmLvXwwqak/gpYnD0NqMB/GIFezoDLUGViPlht\npImfj3eJEmi6v8FRpy/DiDmxPHXLoTl8qdDzcr1jsrlMD3UMj9hZnNw+YaUINoYL\n8yJBL3gRkSIjBbxHSgZkpxdGGMThtAwFPQIDAQAB\n-----END RSA PUBLIC KEY-----"
  },
  "tokenProvider": {
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAiLbPGJrBin4pKe8iHYHnU2ii0MPbwuGqwQLU+ymTGNXc8DPC\nmR31TJ5FAifzndatPdSLy9WgciIzwDySpsklMfJ6uDLm2ecEfTfjYGeK3VJuppS6\n7C08YAHquEAm6ZpxHqX955Hc3LuORYldpp281zZQ02+UizqeqbNBqEiuIMiIldvF\n9ARu5wyL0R7vwHqDAyEt8+7zd4rYmidmLvXwwqak/gpYnD0NqMB/GIFezoDLUGVi\nPlhtpImfj3eJEmi6v8FRpy/DiDmxPHXLoTl8qdDzcr1jsrlMD3UMj9hZnNw+YaUI\nNoYL8yJBL3gRkSIjBbxHSgZkpxdGGMThtAwFPQIDAQABAoIBAQCFX9Mqbc/jDLGb\n92+Q2eqTtMEiafE7bmznX5voa+bVk863h8eRGWj2FnG8aL2Cfq0EZRb9BKxCrJtQ\nL8fibtmaoLSYfxA4BOicQKnSozEWOhU47mBtiLQd9cDHf852l/VDsSzDfcm6jj9+\nF0UGB1zzmEnB/Fkue0HOzNKBGmbGE1/RWo4+co3XYpq/YZdIQQadsV6G2ma1IApm\n5t9AmEzhjMJsVFTZqzWQpoHmJRJjfwqqugmt4gpBCuYMkVcuN0g1olOLobF+pITn\nNjVaYimiFDsjhZyM4c0MFUYB27TeG7RV+QigHM/DW9rTbvTXxnTl/O4mipUe4FsK\nTdsbu4kBAoGBAPwJeudZz0cYM7MPzYE0/ksA9OrJwbhg+BjUJQ7CtFijJp8Cxt3d\ncu6DC5GOke0dXe845DLwriYKqJbae0FIeQvHrFTRpC2gGI5CJ3nPrZ3jbJhQLnQA\nMoF7EbJdpMM0I1xZsZ1cOy7jDbQ6/5Za4RC59xyqZ8hIi8vn9JVAkurPAoGBAIrd\nHyQ0NuZsQCxqNKPvU+1QnpFaoy65yA1kLZ/JDghjTiplxmv0WMuf1B3rq09RvTKK\nCEc7S8CredzLDtgQF7KiENnMno9e8bcMAddC+UaKsV9Eoy4mXGe8vTnQsGvlMkZI\n27gkM1o+Sbsl45Ayr3eYcpJD6K+txSjJYO8B3GIzAoGAUQ5ndYIftHinH95kNDqr\n0clj+yKZ58df4vRPWrjpsVv/LsKA3Je8v9JrZQuaCM0aCbadRXi8OUXSRHnNjAhX\nzZ8Q4FJv37COVSoXcgiFiLK8mRuoZOwvUg8XeOq+83yQJsI96iLgccrZ/G3BB0UA\n/xUf0RtIt1QFibV2po2W8mcCgYBjLOXO35POIcX7cqbB5m3Ucd2uBkPBXWIpXkDP\ne7KP/wyWbzW1aD/6vd2quOQStFghvj+HUCwcINvZ+xRQ771dES5jvyYHU1Hi36p4\n6RZLcUaYudapYTBhzoR+xDMb/AdZ9zMlYoVikFXsWXUbSXfUPIanO+T1g2/qX1jh\nmjyhzQKBgQCvQdWSYMXaOnQRo/BHmRdR3X3aEZyLEn5UmXjxohUeB/Jtzb15FD/r\ny5ljkTVQyzYnRgwBuZBnUl/sCbxRp1CXbDmotHZ3DBwRJuP7KaPoj7dH+xfU6P3N\nuAm6CP+73AONmouW/8tVW60n2IkSH5I1ROYtPLrI6475M/hB4Anj+Q==\n-----END RSA PRIVATE KEY-----",
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAiLbPGJrBin4pKe8iHYHnU2ii0MPbwuGqwQLU+ymTGNXc8DPCmR31\nTJ5FAifzndatPdSLy9WgciIzwDySpsklMfJ6uDLm2ecEfTfjYGeK3VJuppS67C08\nYAHquEAm6ZpxHqX955Hc3LuORYldpp281zZQ02+UizqeqbNBqEiuIMiIldvF9ARu\n5wyL0R7vwHqDAyEt8+7zd4rYmidmLvXwwqak/gpYnD0NqMB/GIFezoDLUGViPlht\npImfj3eJEmi6v8FRpy/DiDmxPHXLoTl8qdDzcr1jsrlMD3UMj9hZnNw+YaUINoYL\n8yJBL3gRkSIjBbxHSgZkpxdGGMThtAwFPQIDAQAB\n-----END RSA PUBLIC KEY-----"
  },
  "client": {
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAsZo2Yoij0v2OsWQAGMx5dSypNhUNV3FLDBspF3ZtY8Id6lqgeAsH\nZGB9fVhivf2KmXe20teUg5d271U2dbbh3g13AwKacVjVGRR/UB/PUNn0K4jkbFZJ\nJp6nJrwsPlBZrEPUlaJSdveEk0/o4d0OyMNkFWT+Ivuj+DJBQdWQhqsIT2jQKKx5\nVFktJ01K7if3DLaH0fHupqj46zY6bZCRAiyyZynL0blnojwG2tUnRtID6zjOkSB8\ncUKOXHy3jhiTEESwpY8FvJc0HNdG/s/5h0OZ1ayFHRhbeunx6Ddn6Yfewbdix0/H\nBgZ8Ycp3q3fUhVBygQcF+rWEdRTpc1x3UwIDAQAB\n-----END RSA PUBLIC KEY-----",
    "privateKey":"-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAsZo2Yoij0v2OsWQAGMx5dSypNhUNV3FLDBspF3ZtY8Id6lqg\neAsHZGB9fVhivf2KmXe20teUg5d271U2dbbh3g13AwKacVjVGRR/UB/PUNn0K4jk\nbFZJJp6nJrwsPlBZrEPUlaJSdveEk0/o4d0OyMNkFWT+Ivuj+DJBQdWQhqsIT2jQ\nKKx5VFktJ01K7if3DLaH0fHupqj46zY6bZCRAiyyZynL0blnojwG2tUnRtID6zjO\nkSB8cUKOXHy3jhiTEESwpY8FvJc0HNdG/s/5h0OZ1ayFHRhbeunx6Ddn6Yfewbdi\nx0/HBgZ8Ycp3q3fUhVBygQcF+rWEdRTpc1x3UwIDAQABAoIBAG0m0AHi5HyJ5SbU\nxNJ46LTBDAX3DkoNkQgRsUGAQV6sMOKVbEYa0SI5wyZEKfuGVaZYUv5mDBqL/ka9\nYkkaatqj/97TvMOoyt8tH8nIowuXyF7xNSH4LeEUZLqDr9VkYJhk38RPDMuc99fp\npsEZsTpmC56ygRZS/cVObZjefoKkl+2cPL0jfSWUSn2t/nUKbmwNbjbM6f1OCCyJ\n814LZalfeLh+hfEdY8yVK0Rlaut/FLw4eaY43H0QTwDyYXmgOgYckia7MQmtu5yO\n0dFRDZa7x0j01w0+Hphr88bhtXFgv8VPWyzyz2d8fNWlbMP4JCARes3lVpkMKHDn\nFZIULcECgYEA53qsuOQrE3xHtNzfak7FjGgkQTxi4TeYXRJ5Zhlkjtb9nQnsFzTq\n+AXpeHfsGmaza9/LcMUJ9JzYV7t5V8T6vZT3riOnjQAfbHOrR3EYK5/vv4ZfjFgk\nBySoNjOtf67ZXn1lRJ3FZNN4RfGmh/2aqYi5PAn1g5+nLXtMIY88a1ECgYEAxGp9\nW03pH4QwBzvFA9PJ+BhWXS8fAcafY67TlXg+IxzGP/YQupodovDChXpvbfYV0kf6\nU6YddDQT5p0j2YmssJCaDa93iMXU2/JgI5pcTNFxLa5LR/Mg30JYyniK/4+YIkoL\n68V2OH0Y65DIQdajHPojzFBmDSBZSnkyO9Ojx2MCgYADCoSMZUr/lYlnoeM5hVFp\nF9EqHj36UX2p810u7zR3//ETCBdW8rYHjiRUFdc/PYwr5aPJln0b/peFB4x/j7Hv\nna5nVkaUPqUrCpX8eUrk/9Ppgz1sHZhTk7K2C5XC8KwgZqtW7G+0dGbHHHagoL9Q\nbOBqHoNgOE+89Dq60iPsEQKBgByahXbueaylS3lCMwbDqP4ATVN0sUdI7Z1OsHFr\n+WCTqCtYYkdKelZoSWu20NNqqvLcmI/l+RQbIWrMJ5RegE+WP1kO3JGGfeEqAuYs\nbJSjS6AjacMonPjmaJfTxipBdx5HOkUzlGvVi/OCOiecYlSt+NigPLxcoaQ+0hn0\nUD2RAoGAUH2QV31iSFLs+ByQdGmPaYuVxhNUwaGoN0M5splIdH6DAgyIvM5ogXVb\nMV3yUdnULU+5YeL8r93NeFRHiBCBfvcIcDdJHxRJNQFvdsAWosGeXI6clC1YkUnB\n2N+9PrizN87XUb6NmY07NwtAKrU6aZ0XwKUEgor9rntT8IawFo4=\n-----END RSA PRIVATE KEY-----"
  },
  "stack": {
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAsZo2Yoij0v2OsWQAGMx5dSypNhUNV3FLDBspF3ZtY8Id6lqgeAsH\nZGB9fVhivf2KmXe20teUg5d271U2dbbh3g13AwKacVjVGRR/UB/PUNn0K4jkbFZJ\nJp6nJrwsPlBZrEPUlaJSdveEk0/o4d0OyMNkFWT+Ivuj+DJBQdWQhqsIT2jQKKx5\nVFktJ01K7if3DLaH0fHupqj46zY6bZCRAiyyZynL0blnojwG2tUnRtID6zjOkSB8\ncUKOXHy3jhiTEESwpY8FvJc0HNdG/s/5h0OZ1ayFHRhbeunx6Ddn6Yfewbdix0/H\nBgZ8Ycp3q3fUhVBygQcF+rWEdRTpc1x3UwIDAQAB\n-----END RSA PUBLIC KEY-----",
    "privateKey":"-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAsZo2Yoij0v2OsWQAGMx5dSypNhUNV3FLDBspF3ZtY8Id6lqg\neAsHZGB9fVhivf2KmXe20teUg5d271U2dbbh3g13AwKacVjVGRR/UB/PUNn0K4jk\nbFZJJp6nJrwsPlBZrEPUlaJSdveEk0/o4d0OyMNkFWT+Ivuj+DJBQdWQhqsIT2jQ\nKKx5VFktJ01K7if3DLaH0fHupqj46zY6bZCRAiyyZynL0blnojwG2tUnRtID6zjO\nkSB8cUKOXHy3jhiTEESwpY8FvJc0HNdG/s/5h0OZ1ayFHRhbeunx6Ddn6Yfewbdi\nx0/HBgZ8Ycp3q3fUhVBygQcF+rWEdRTpc1x3UwIDAQABAoIBAG0m0AHi5HyJ5SbU\nxNJ46LTBDAX3DkoNkQgRsUGAQV6sMOKVbEYa0SI5wyZEKfuGVaZYUv5mDBqL/ka9\nYkkaatqj/97TvMOoyt8tH8nIowuXyF7xNSH4LeEUZLqDr9VkYJhk38RPDMuc99fp\npsEZsTpmC56ygRZS/cVObZjefoKkl+2cPL0jfSWUSn2t/nUKbmwNbjbM6f1OCCyJ\n814LZalfeLh+hfEdY8yVK0Rlaut/FLw4eaY43H0QTwDyYXmgOgYckia7MQmtu5yO\n0dFRDZa7x0j01w0+Hphr88bhtXFgv8VPWyzyz2d8fNWlbMP4JCARes3lVpkMKHDn\nFZIULcECgYEA53qsuOQrE3xHtNzfak7FjGgkQTxi4TeYXRJ5Zhlkjtb9nQnsFzTq\n+AXpeHfsGmaza9/LcMUJ9JzYV7t5V8T6vZT3riOnjQAfbHOrR3EYK5/vv4ZfjFgk\nBySoNjOtf67ZXn1lRJ3FZNN4RfGmh/2aqYi5PAn1g5+nLXtMIY88a1ECgYEAxGp9\nW03pH4QwBzvFA9PJ+BhWXS8fAcafY67TlXg+IxzGP/YQupodovDChXpvbfYV0kf6\nU6YddDQT5p0j2YmssJCaDa93iMXU2/JgI5pcTNFxLa5LR/Mg30JYyniK/4+YIkoL\n68V2OH0Y65DIQdajHPojzFBmDSBZSnkyO9Ojx2MCgYADCoSMZUr/lYlnoeM5hVFp\nF9EqHj36UX2p810u7zR3//ETCBdW8rYHjiRUFdc/PYwr5aPJln0b/peFB4x/j7Hv\nna5nVkaUPqUrCpX8eUrk/9Ppgz1sHZhTk7K2C5XC8KwgZqtW7G+0dGbHHHagoL9Q\nbOBqHoNgOE+89Dq60iPsEQKBgByahXbueaylS3lCMwbDqP4ATVN0sUdI7Z1OsHFr\n+WCTqCtYYkdKelZoSWu20NNqqvLcmI/l+RQbIWrMJ5RegE+WP1kO3JGGfeEqAuYs\nbJSjS6AjacMonPjmaJfTxipBdx5HOkUzlGvVi/OCOiecYlSt+NigPLxcoaQ+0hn0\nUD2RAoGAUH2QV31iSFLs+ByQdGmPaYuVxhNUwaGoN0M5splIdH6DAgyIvM5ogXVb\nMV3yUdnULU+5YeL8r93NeFRHiBCBfvcIcDdJHxRJNQFvdsAWosGeXI6clC1YkUnB\n2N+9PrizN87XUb6NmY07NwtAKrU6aZ0XwKUEgor9rntT8IawFo4=\n-----END RSA PRIVATE KEY-----"
  }
};

describe('Stack IO', function () {
  it('requires a stack', function () {
    assert.throws(
      function () {
        new StackIO();
      },
      /stack is required/
    );
  });
  
  context('with stack', function () {
    var stack;
    
    beforeEach(function () {
      stack = new o2.Stack(testData.stack.privateKey);
    });
    
    it('accepts stack', function () {
      new StackIO(stack);
    });
  });
  
  context('once instantiated', function () {
    var stack;
    var stackIo;
    
    beforeEach(function () {
      stack = new o2.Stack(testData.stack.privateKey);
      stackIo = new StackIO(stack);
    });
    
    describe('.getStack', function () {
      it('returns stack', function () {
        assert.equal(stackIo.getStack(), stack);
      });
    });
    
    describe('.authorizePlugin', function () {
      it('requires a plugin name', function () {
        assert.throws(
          function () {
            stackIo.authorizePlugin();
          },
          /plugin name is required/
        );
      });
      
      it('requires a plugin name to be a string', function () {
        assert.throws(
          function () {
            stackIo.authorizePlugin({});
          },
          /plugin name must be a string/
        );
      });
      
      it('requires a public key', function () {
        assert.throws(
          function () {
            stackIo.authorizePlugin('Plugin1');
          },
          /public key is required/
        );
      });
      
      it('requires public key to be a string', function () {
        assert.throws(
          function () {
            stackIo.authorizePlugin('Plugin1', {});
          },
          /public key must be a string/
        );
      });
      
      it('authorizes plugin in name registry', function () {
        stackIo.authorizePlugin('Plugin1', testData.plugin.publicKey);
        
        assert.equal(
          stackIo.getNameRegistry().getNamespacePublicKey('Plugin.Plugin1'),
          testData.plugin.publicKey
        );
      });
    });
    
    describe('.authorizeTokenProvider', function () {
      it('requires a token provider name', function () {
        assert.throws(
          function () {
            stackIo.authorizeTokenProvider();
          },
          /token provider name is required/
        );
      });
      
      it('requires a token provider name to be a string', function () {
        assert.throws(
          function () {
            stackIo.authorizeTokenProvider({});
          },
          /token provider name must be a string/
        );
      });
      
      it('requires a public key', function () {
        assert.throws(
          function () {
            stackIo.authorizeTokenProvider('TP1');
          },
          /public key is required/
        );
      });
      
      it('requires public key to be a string', function () {
        assert.throws(
          function () {
            stackIo.authorizeTokenProvider('TP1', {});
          },
          /public key must be a string/
        );
      });
      
      it('authorizes token provider in name registry', function () {
        stackIo.authorizeTokenProvider('TP1', testData.tokenProvider.publicKey);
        
        assert.equal(
          stackIo.getNameRegistry().getNamespacePublicKey('TokenProvider.TP1'),
          testData.tokenProvider.publicKey
        );
      });
    });
  });
});