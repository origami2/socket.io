var o2 = require('origami2-core');

var Crane = o2.Crane;
var CryptoUtils = o2.CryptoUtils;
var NameRegistry = o2.NameRegistry;
var Client = o2.Client;
var PenelopeTokenProvider = o2.PenelopeTokenProvider;
var RSASocket = o2.RSASocket;

function ClientIO(privateKey, events) {
    this.rsaSocket = new RSASocket(privateKey);
    this.nameRegistry = new NameRegistry(privateKey);
    this.privateKey = privateKey;
    this.publicKey = CryptoUtils.asString(CryptoUtils.asPrivateKey(privateKey));
    this.rsaSocket = new RSASocket(privateKey);
}

ClientIO.prototype.connect = function (url, token) {
    var self = this;

    return new Promise(function (resolve, reject) {
        var socket = require('socket.io-client')(url);

        socket
            .on(
                'connect',
                function () {
                    self
                        .listenSocket(socket, token)
                        .then(resolve)
                        .catch(reject);
                }
            );
    });
};

ClientIO.prototype.listenSocket = function (socket, token) {
    var self = this;

    return new Promise(function (resolve, reject) {
        self
            .rsaSocket
            .connect(socket)
            .then(function (secureSocket) {
                var clientFactory = new o2.ClientFactory();

                var crane = new Crane(
                    self.nameRegistry,
                    function () {
                        // no incoming connections
                        return Promise.reject();
                    },
                    secureSocket
                );

                crane
                    .createSocket(
                        'Client',
                        (socket, params) => clientFactory.createClient(socket, token).then((client) => resolve(client)),
                        {
                        }
                    )
                    .then(() => {})
                    .catch(reject);
            });
    });
};

module.exports = ClientIO;