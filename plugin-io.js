var o2 = require('origami2-core');

var Crane = o2.Crane;
var NameRegistry = o2.NameRegistry;
var Plugin = o2.Plugin;
var PenelopePlugin = o2.PenelopePlugin;
var RSASocket = o2.RSASocket;

function PluginIO(api, privateKey, dependenciesMap, locals, events) {
  this.plugin = new Plugin(
    api,
    function () {
      return Promise.resolve(locals || {});
    }
  );
  
  this.privateKey = privateKey;
  this.dependenciesMap = dependenciesMap || {};
  this.locals = locals || {};
  this.rsaSocket = new RSASocket(privateKey);
  this.nameRegistry = new NameRegistry(privateKey);
  this.events = events || {};
}

PluginIO.prototype.connect = function (url) {
  var self = this;

  return new Promise(function (resolve, reject) {
    var socket = require('socket.io-client')(url);
    
    socket
    .on(
      'connect',
      function () {
        self
        .listenSocket(socket)
        .then(resolve)
        .catch(reject);
      }
    );
  });
};

PluginIO.prototype.listenSocket = function (socket) {
  var self = this;
  
  return new Promise(function (resolve, reject) {
    self
    .rsaSocket
    .connect(socket)
    .then(function (secureSocket) {
      var penelopePlugin = new PenelopePlugin(
        // plugin
        self.plugin,
        // locals
        self.locals,
        // depenedencies
        self.dependenciesMap,
        // events
        self.events
      );
      
      var crane = new Crane(
        self.nameRegistry,
        function () {
          // no incoming connections
          return Promise.reject();
        },
        secureSocket
      );
      
      var eventNames = Object.keys(self.events);
      
      crane
      .createSocket(
        'Plugin.' + self.plugin.getName(),
        penelopePlugin,
        {
          methods: self.plugin.describeMethods(),
          name: self.plugin.getName(),
          events: eventNames
        }
      )
      .then(resolve)
      .catch(reject);
    });
  });
};

module.exports = PluginIO;