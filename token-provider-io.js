var o2 = require('origami2-core');

var Crane = o2.Crane;
var CryptoUtils = o2.CryptoUtils;
var NameRegistry = o2.NameRegistry;
var Plugin = o2.Plugin;
var PenelopeTokenProvider = o2.PenelopeTokenProvider;
var RSASocket = o2.RSASocket;

function TokenProviderIO(api, privateKey, dependenciesMap, locals) {
  this.plugin = new Plugin(
    api,
    function () {
      return Promise.resolve(locals || {});
    }
  );
  
  this.privateKey = privateKey;
  this.publicKey = CryptoUtils.asString(CryptoUtils.asPrivateKey(privateKey));
  this.dependenciesMap = dependenciesMap || {};
  this.locals = locals || {};
  this.rsaSocket = new RSASocket(privateKey);
  this.nameRegistry = new NameRegistry(privateKey);
}

TokenProviderIO.prototype.connect = function (url) {
  var self = this;

  return new Promise(function (resolve, reject) {
    var socket = require('socket.io-client')(url);
    
    socket
    .on(
      'connect',
      function () {
        self
        .listenSocket(socket)
        .then(resolve)
        .catch(reject);
      }
    );
  });
};

TokenProviderIO.prototype.listenSocket = function (socket) {
  var self = this;
  
  return new Promise(function (resolve, reject) {
    self
    .rsaSocket
    .connect(socket)
    .then(function (secureSocket) {
      var crane = new Crane(
        self.nameRegistry,
        function () {
          // no incoming connections
          return Promise.reject();
        },
        secureSocket
      );
      
      crane
      .createSocket(
        'TokenProvider.' + self.plugin.getName(),
        new PenelopeTokenProvider(
          // plugin
          self.plugin,
          // locals
          self.locals,
          // depenedencies
          self.dependenciesMap
        ),
        {
          publicKey: self.publicKey,
          methods: self.plugin.describeMethods(),
          name: self.plugin.getName()
        }
      )
      .then(resolve)
      .catch(reject);
    });
  });
};

module.exports = TokenProviderIO;