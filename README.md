# Origami 2 stack socket.io helper

## constructor

Params:

- `privateKey` **requred**: stack private key

## .getStack

Returns: underlying *stack* instance

## .getNameRegistry

Returns: underlying *name registry* instance

## .authorizePlugin

Params:

- `plugin name` **required**: plugin name
- `public key` **required**: plugin public key

## .authorizeTokenProvider

Params:

- `plugin name` **required**: token provider name
- `public key` **required**: token provider public key

## .listenServer

Creates a [socket.io](http://socket.io/) that listens to the node.js http or https server and attaches to it using `.listenIo`.

Params:

- `server` **required**: http or https node.js style server

## .listenIo

Attaches to a [socket.io instance](http://socket.io/) object. For each connected socket, invokes `.listenSocket`.

Params:

- `io` **required**: [socket.io instance](http://socket.io/) object instance

## .listenSocket

Handles incoming connections on the socket instance. It is a node.js event-emitter or a socket.io style socket object.

Params:

- `socket` **required**: socket to attach the stack to

# Origami 2 plugin socket.io helper

## constructor

Params:

- `api` **required**: Plugin constructor function.
- `privateKey` **required**: Plugin private key.
- `dependenciesMap` **optional**: An object containing string keys and string values. For example:

```
{
  "constructorParameterName": "OtherPluginName"
}
```

  A client object will be created, that client will use the same context as the plugin invoked method.
  Each key like `constructorParameterName` will be mapped to an API proxy.

- `locals` **optional**: An object containing string keys and string values. For example:

```
{
  "param1": 1,
  "param2": "another",
  "db": myDb
}
```
- `events` **optional**:

  Each key like `param1` will be set as the value on the constructor of the same name.
  
> An additional constructor will be passed: `emit` which receives one parameter and broadcast the event to the stack.

## .connect

Returns: a Promise to be resolved once connected
Params:
- `url` **required**: the URL to connect using a [socket.io client](http://socket.io).

> It is expected the other end to be using the [stack socket.io helper](https://gitlab.com/origami2/stack-io)

## .listenSocket

Returns: a Promise to be resolved once connected
Params:
- `socket` **required**: a socket to connect to. Might be an event emitter or a [socket.io socket object](http://socket.io).

> It is expected the other end to be using the [stack socket.io helper](https://gitlab.com/origami2/stack-io)


