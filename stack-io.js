var o2 = require('origami2-core');
var Crane = o2.Crane;
var NameRegistry = o2.NameRegistry;
var Stack = o2.Stack;
var PenelopeStack = o2.PenelopeStack;
var RSASocket = o2.RSASocket;

function StackIO(stack) {
  if (!stack) throw new Error('stack is required');
  
  this.stack = stack;
  
  var privateKey = this.privateKey = stack.getPrivateKey();
  this.rsaSocket = new RSASocket(privateKey);
  this.nameRegistry = new NameRegistry(privateKey);
  this.nameRegistry.addPublicNamespace('Events');
  this.nameRegistry.addPublicNamespace('Client');
  this.penelopeStack = new PenelopeStack(
    this.stack
  );
}

StackIO.prototype.getStack = function () {
  return this.stack;
};

StackIO.prototype.getNameRegistry = function () {
  return this.nameRegistry;
};

StackIO.prototype.authorizePlugin = function (name, publicKey) {
  if (!name) throw new Error('plugin name is required');
  if (typeof(name) !== 'string') throw new Error('plugin name must be a string');
  
  if (!publicKey) throw new Error('public key is required');
  if (typeof(publicKey) !== 'string') throw new Error('public key must be a string');
  
  this.nameRegistry.authorizeNamespace('Plugin.' + name, publicKey);
};

StackIO.prototype.authorizeTokenProvider = function (name, publicKey) {
  if (!name) throw new Error('token provider name is required');
  if (typeof(name) !== 'string') throw new Error('token provider name must be a string');
  
  if (!publicKey) throw new Error('public key is required');
  if (typeof(publicKey) !== 'string') throw new Error('public key must be a string');
  
  this.nameRegistry.authorizeNamespace('TokenProvider.' + name, publicKey);
};

StackIO.prototype.listenServer = function (server) {
  var io = require('socket.io')(server);
  
  this.listenIo(io);
};

StackIO.prototype.listenIo = function (io) {
  var self = this;
  
  io
  .on(
    'connect',
    function (socket) {
      self
      .listenSocket(socket);
    }
  );
};

StackIO.prototype.listenSocket = function (socket) {
  var self = this;
  
  self
  .rsaSocket
  .connect(socket)
  .then(function (secureSocket) {
    new Crane(
      self.nameRegistry,
      self.penelopeStack,
      secureSocket
    );
  });
};

module.exports = StackIO;